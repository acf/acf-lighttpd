local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")

-- Set variables
local processname = "lighttpd"
local packagename = "lighttpd"
local filelist = {"/etc/lighttpd/lighttpd.conf", "/etc/lighttpd/mod_cgi.conf", "/etc/lighttpd/mod_fastcgi.conf", "/etc/lighttpd/mod_fastcgi_fpm.conf"}

local path = "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin "

-- ################################################################################
-- LOCAL FUNCTIONS

local function replacetags(configtable, name, tags)
	tags = tags or {}
	if tags[name] then return tags[name] end

	local value = configtable[name]
	if not value or value == "" then
		tags[name] = ""
		return ""
	elseif tonumber(value) then
		tags[name] = tonumber(value)
		return tonumber(value)
	end

	-- Split the value into fields based upon '+'
	local fields = {}
	local pos=1
	while pos <= string.len(value) do
		if string.find(value, "^%s-\"", pos) then
			local start = string.find(value, "\"", pos)
			local stop = string.find(value, "\"", start+1)
			fields[#fields+1] = string.sub(value, start+1, stop-1)
			pos = string.find(value, "[^%+%s]", stop+1) or stop+1
		elseif string.find(value, "%+", pos) then
			local start = string.find(value, "%S", pos)
			local stop = string.find(value, "%s-%+", start+1)
			local tag = string.sub(value, start, stop-1)
			fields[#fields+1] = replacetags(configtable, tag, tags)
			pos = string.find(value, "[^%+%s]", stop+1) or stop+1
		else
			local tag = string.match(value, "%S.*", pos)
			fields[#fields+1] = replacetags(configtable, tag, tags)
			break
		end
	end
	value = table.concat(fields)
	tags[name] = value
	return value
end

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Lighttpd Status")
end

function mymodule.getfilelist()
	local listed_files = {}

	for i,name in ipairs(filelist) do
		local filedetails = posix.stat(name) or {}
		filedetails.filename=name
		table.insert(listed_files, filedetails)
	end

	table.sort(listed_files, function (a,b) return (a.filename < b.filename) end )

	return cfe({ type="structure", value=listed_files, label="Lighttpd File List" })
end

function mymodule.getfiledetails(filename)
	return modelfunctions.getfiledetails(filename, filelist)
end

function mymodule.updatefiledetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, filelist)
end

function mymodule.get_logfile(self, clientdata)
	--[[
	server.errorlog			pathname of the error-log
	server.errorlog-use-syslog	send errorlog to syslog
	accesslog.use-syslog		send the accesslog to syslog
	accesslog.filename		name of the file where the accesslog should be written to if syslog is not used
	--]]

	local retval = cfe({ type="structure", value={}, label="Log File Configuration" })
	local config = format.parse_ini_file(fs.read_file(filelist[1]), "")
	local syslogerror = replacetags(config, "server.errorlog-use-syslog")
	local syslogaccess = replacetags(config, "accesslog.use-syslog")

	if syslogerror == "enable" or syslogaccess == "enable" then
		retval.value[#retval.value+1] = {facility="daemon", grep="lighttpd"}
	end

	if syslogaccess ~= "enable" then
		local accessfile = replacetags(config, "accesslog.filename") or "/var/log/lighttpd/access.log"
		retval.value[#retval.value+1] = {filename=accessfile}
	end

	if syslogerror ~= "enable" then
		local errorfile = replacetags(config, "server.errorlog") or "/var/log/lighttpd/error.log"
		retval.value[#retval.value+1] = {filename=errorfile}
	end

	return retval
end

return mymodule
